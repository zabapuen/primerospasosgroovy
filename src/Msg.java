public class Msg {

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    void addContent(String content) {
        this.content = this.content + " " + content;
    }

    private String content;
}
