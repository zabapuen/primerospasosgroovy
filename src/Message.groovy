class Message {
    private def content

    def getContent() {
        return content
    }

    void setContent(content) {
        this.content = content
    }
}
